import React from "react"

class HatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            locations: [],
            picture: "",
            color: "",
            style: "",
            fabric: "",
        };

        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handlePictureChange = this.handlePictureChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;

        const hatUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {

            const cleared = {
                picture: "",
                color: "",
                style: "",
                fabric: "",
                location: "",
            }
            this.setState(cleared)
        } else {
            console.error(response)
        }
    }
        
    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({fabric: value});
    }

    handleStyleChange(event) {
        const value = event.target.value;
        this.setState({style: value})
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value})
    }

    handlePictureChange(event) {
        const value = event.target.value;
        this.setState({picture: value})
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value})
    }

    async componentDidMount() {
        const url = "http://localhost:8090/api/locations/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();

            this.setState({"locations": data.locations})
        }
    }

    render (){
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new hat!</h1>
                        <form onSubmit={this.handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleFabricChange} placeholder="Fabric"
                                    required type="text" id="fabric" className="form-control"
                                    name="fabric" value={this.state.fabric}/>
                                <label htmlFor="name">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleStyleChange} placeholder="Style"
                                    required type="text" id="style" className="form-control"
                                    name="style" value={this.state.style}/>
                                <label htmlFor="name">Style</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleColorChange} placeholder="Color"
                                    required type="text" id="color" className="form-control"
                                    name="color" value={this.state.color}/>
                                <label htmlFor="name">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePictureChange} placeholder="Picture"
                                    required type="text" id="picture" className="form-control"
                                    name="picture" value={this.state.picture}/>
                                <label htmlFor="name">Picture</label>
                            </div>
                
                            <div className="mb-3">
                                <select onChange={this.handleLocationChange} required id="location"
                                    className="form-select" name="location" value={this.state.location}>
                                <option value="">Choose hat location</option>
                                {this.state.locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>
                                            {location.closet_name} closet - shelf: {location.shelf_number} section: {location.section_number}
                                        </option>
                                    );
                                })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create Hat</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default HatForm;