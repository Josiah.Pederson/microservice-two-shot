from django.db import models
from django.urls import reverse

# Create your models here.

class LocationVO(models.Model):
    shelf_number = models.SmallIntegerField()
    section_number = models.SmallIntegerField()
    closet_name = models.CharField(max_length=40)
    id_number = models.SmallIntegerField(unique=True, null=True)
    import_href = models.CharField(max_length=200, unique=True, null=True)

    def __str__(self):
        return f"location number: {self.id_number} {self.closet_name}"

class Hat(models.Model):
    fabric = models.CharField(max_length=25)
    style = models.CharField(max_length=30)
    color = models.CharField(max_length=20)
    picture = models.URLField(null=True, blank=True)
    location = models.OneToOneField(LocationVO, related_name="hats", on_delete=models.PROTECT)

    def get_api_url(self):
        return reverse("api_detail_hat", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.color} {self.fabric} hat in {self.location}"