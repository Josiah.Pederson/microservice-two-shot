# Wardrobify

Team:

* Josiah Pederson - Hats
* Yehsun Kang - Shoes

## Design

## Shoes microservice

The shoes app has bins. Each has closet name, bin number, and bin size.
Each bin can keep a shoe with manufacturer, model name, model coler, picture, and its own unique ide.
That way the user can track of shoes inventory.

## Hats microservice

For some reason, this person who is using the app has multiple
closets which are named. In each (weirdly) named closet there are
shelves and sections on each shelf. One hat per section. (onetoone)
The hats can be any size, color or whatever but the important part is 
that hats do not stack. This is because the hats might get damaged.
Partitioned, cleanly, this persons seemingly limitless hats will not
only be safe but easy to find.